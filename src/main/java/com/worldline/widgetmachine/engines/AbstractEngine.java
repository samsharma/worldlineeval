package com.worldline.widgetmachine.engines;

import java.math.BigDecimal;
import java.util.HashSet;

import com.worldline.widgetmachine.common.FuelType;

public abstract class AbstractEngine {

	private final FuelType requiredFuelType;

	private int currentFuelLevel;
	private FuelType currentFuelType;
	private boolean isRunning;

	public AbstractEngine(FuelType requiredFuelType) throws IllegalStateException {

		if (this.getSupportedFuelTypes().contains(requiredFuelType)) {
			this.currentFuelLevel = 0;
			this.requiredFuelType = requiredFuelType;
			this.isRunning = false;
		} else {
			throw new IllegalStateException(
					this.getEngineTypeName() + " cannot be initialized with fuel type " + requiredFuelType
							+ ". Supported fuel types are:" + System.lineSeparator() + this.getSupportedFuelTypes());
		}
	}

	public abstract String getEngineTypeName();

	public abstract HashSet<FuelType> getSupportedFuelTypes();
	
	public abstract long getBatchSize();

	public BigDecimal getCostPerBatch() {
		return this.requiredFuelType.getCostPerBatch();
	}

	public void start() throws IllegalStateException {

		if (!(this.currentFuelLevel > 0)) {
			throw new IllegalStateException("No fuel. Please refuel first.");
		}

		if (!this.currentFuelType.equals(this.requiredFuelType)) {

			throw new IllegalStateException(
					"This " + this.getEngineTypeName() + " cannot run on " + this.currentFuelType
							+ ". Please empty the fuel and refill with " + this.requiredFuelType + ", and then retry.");
		}

		this.isRunning = true;
	}

	public void stop() {
		this.isRunning = false;
	}

	public boolean isRunning() {
		return this.isRunning;
	}

	public void fillFuel(FuelType fuelType, int fuelQuantity) {

		if (fuelQuantity >= 0 && fuelQuantity <= 100) {
			this.currentFuelLevel = fuelQuantity;
		} else if (fuelQuantity > 100) {
			this.currentFuelLevel = 100;
		} else {
			this.currentFuelLevel = 0;
		}

		this.currentFuelType = fuelType;
	}

	public void emptyFuel() {

		this.currentFuelLevel = 0;
	}

	public FuelType getRequiredFuelType() {
		return this.requiredFuelType;
	}
}
