package com.worldline.widgetmachine.engines;

import java.util.Arrays;
import java.util.HashSet;

import com.worldline.widgetmachine.common.FuelType;

public final class InternalCombustionEngine extends AbstractEngine {

	private static final String ENGINE_TYPE_NAME = "Internal Combustion Engine";
	
	private static final HashSet<FuelType> SUPPORTED_FUEL_TYPES = new HashSet<>(
			Arrays.asList(FuelType.PETROL, FuelType.DIESEL));
	
	private static final int BATCH_SIZE = 8;

	public InternalCombustionEngine(FuelType requiredFuelType) throws IllegalStateException {
		
		super(requiredFuelType);
	}

	@Override
	public String getEngineTypeName() {
		return ENGINE_TYPE_NAME;
	}

	@Override
	public HashSet<FuelType> getSupportedFuelTypes() {
		return SUPPORTED_FUEL_TYPES;
	}

	@Override
	public long getBatchSize() {
		return BATCH_SIZE;
	}

}
