package com.worldline.widgetmachine.engines;

import java.util.Arrays;
import java.util.HashSet;

import com.worldline.widgetmachine.common.FuelType;

public final class SteamEngine extends AbstractEngine {

	private static final String ENGINE_TYPE_NAME = "Steam Engine";

	private static final HashSet<FuelType> SUPPORTED_FUEL_TYPES = new HashSet<>(
			Arrays.asList(FuelType.COAL, FuelType.WOOD));

	private static final int BATCH_SIZE = 2;

	public SteamEngine(FuelType requiredFuelType) throws IllegalStateException {

		super(requiredFuelType);
	}

	@Override
	public String getEngineTypeName() {
		return ENGINE_TYPE_NAME;
	}
	
	@Override
	public HashSet<FuelType> getSupportedFuelTypes() {
		return SUPPORTED_FUEL_TYPES;
	}
	
	@Override
	public long getBatchSize() {
		return BATCH_SIZE;
	}
}