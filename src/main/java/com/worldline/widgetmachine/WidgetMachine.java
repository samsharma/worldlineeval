package com.worldline.widgetmachine;

import java.math.BigDecimal;

import com.worldline.widgetmachine.common.FuelType;
import com.worldline.widgetmachine.engines.AbstractEngine;

public class WidgetMachine {

	private AbstractEngine engine;

	public WidgetMachine(AbstractEngine engine) {
		this.engine = engine;
	}

	public void changeEngine(AbstractEngine newEngine) throws IllegalStateException {
		if (!this.engine.isRunning()) {
			this.engine = newEngine;
		} else {
			throw new IllegalStateException("Cannot change the engine while the engine is running. "
					+ "Please try again after the current production job has finished.");
		}
	}

	public void addFuelToEngine(FuelType fuelType, int fuelLevel) {
		this.engine.fillFuel(fuelType, fuelLevel);
	}

	public void emptyEngineFuel() {
		this.engine.emptyFuel();
	}

	public BigDecimal produceWidgets(long numberOfWidgetsToBeProduced) throws IllegalStateException {

		if (numberOfWidgetsToBeProduced < 1) {
			throw new IllegalStateException("Widgets to be produced must be non-zero a positive integer.");
		}

		this.engine.start();

		BigDecimal cost = null;

		if (this.engine.isRunning()) {
			cost = produce(numberOfWidgetsToBeProduced);
		}

		this.engine.stop();

		return cost;
	}

	private BigDecimal produce(long totalNumberOfWidgetsToBeProduced) {

		long numberOfWidgetsAlreadyProduced = 0;
		long numberOfBatchesProduced = 0;

		while (numberOfWidgetsAlreadyProduced < totalNumberOfWidgetsToBeProduced) {
			numberOfWidgetsAlreadyProduced += this.engine.getBatchSize();
			numberOfBatchesProduced++;
		}

		BigDecimal totalCostOfThisRun = this.engine.getCostPerBatch().multiply(new BigDecimal(numberOfBatchesProduced));

		return totalCostOfThisRun;
	}

}
