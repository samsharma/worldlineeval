package com.worldline.widgetmachine.common;

import java.math.BigDecimal;

public enum FuelType {
	

	PETROL(new BigDecimal("9")), DIESEL(new BigDecimal("12")), COAL(new BigDecimal("5.65")), WOOD(new BigDecimal("4.35"));

	private FuelType(BigDecimal costPerBatch) {
		this.costPerBatch = costPerBatch;
	}

	private BigDecimal costPerBatch;

	public BigDecimal getCostPerBatch() {
		return costPerBatch;
	}
}
