package com.worldline.interview.widgetmachine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.worldline.interview.widgetmachine.engines.CommonEnginePositiveTests;
import com.worldline.interview.widgetmachine.engines.InternalCombustionEngineNegativeTests;
import com.worldline.interview.widgetmachine.engines.SteamEngineNegativeTests;

@RunWith(Suite.class)
@SuiteClasses({ SteamEngineNegativeTests.class, InternalCombustionEngineNegativeTests.class,
		CommonEnginePositiveTests.class })
public class WidgetMachineTest {

}