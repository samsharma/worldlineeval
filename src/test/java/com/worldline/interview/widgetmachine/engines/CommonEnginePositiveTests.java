package com.worldline.interview.widgetmachine.engines;

import static com.worldline.widgetmachine.common.FuelType.COAL;
import static com.worldline.widgetmachine.common.FuelType.DIESEL;
import static com.worldline.widgetmachine.common.FuelType.PETROL;
import static com.worldline.widgetmachine.common.FuelType.WOOD;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.worldline.widgetmachine.WidgetMachine;
import com.worldline.widgetmachine.engines.AbstractEngine;
import com.worldline.widgetmachine.engines.InternalCombustionEngine;
import com.worldline.widgetmachine.engines.SteamEngine;

import junit.framework.TestCase;

@RunWith(Parameterized.class)
public class CommonEnginePositiveTests extends TestCase {

	protected int fuelLevel;

	@Before
	public void setUp() {
		this.fuelLevel = 50;
	}

	@Parameter(0)
	public AbstractEngine engine;

	@Parameter(1)
	public long numberOfWidgetsToBeProduced;

	@Parameter(2)
	public BigDecimal expectedCost;

	@Parameters
	public static Collection<Object[]> data() {
//        Object[][] data = new Object[][] { { 1 , 2, 2 }, { 5, 3, 15 }, { 121, 4, 484 } };

		List<Object[]> data = new ArrayList<>();

		AbstractEngine internalCombustionEngineWithPetrol = new InternalCombustionEngine(PETROL);
		AbstractEngine internalCombustionEngineWithDiesel = new InternalCombustionEngine(DIESEL);
		AbstractEngine steamEngineWithWood = new SteamEngine(WOOD);
		AbstractEngine steamEngineWithCoal = new SteamEngine(COAL);

		AbstractEngine[] enginesToTest = new AbstractEngine[] { steamEngineWithWood, steamEngineWithCoal,
				internalCombustionEngineWithPetrol, internalCombustionEngineWithDiesel };

		long[] productionQuantitiesToTest = new long[] { Long.MIN_VALUE, 0, 1, 2, 3, 51, 10000, 100001 };

		for (AbstractEngine currentEngine : enginesToTest) {
			for (long quantity : productionQuantitiesToTest) {
				data.add(new Object[] { currentEngine, quantity,
						new BigDecimal(Math.ceil((double) quantity / (double) currentEngine.getBatchSize()))
								.multiply(currentEngine.getRequiredFuelType().getCostPerBatch()) });
			}
		}

		return data;
	}

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testSteamEngineProductionWithSupportedFuel() {

		this.setName(engine.getClass().getSimpleName() + " with " + engine.getRequiredFuelType() + ", Quantity: "
				+ numberOfWidgetsToBeProduced + ", Expected Cost: " + expectedCost);

//		System.out.println(this.getName());

		WidgetMachine widgetMachine = new WidgetMachine(engine);
		widgetMachine.addFuelToEngine(engine.getRequiredFuelType(), fuelLevel);

		if (numberOfWidgetsToBeProduced < 1) {
			expectedException.expect(IllegalStateException.class);
			expectedException.expectMessage("");
			widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
		} else {
			assertEquals(expectedCost, widgetMachine.produceWidgets(numberOfWidgetsToBeProduced));
		}
	}
}
