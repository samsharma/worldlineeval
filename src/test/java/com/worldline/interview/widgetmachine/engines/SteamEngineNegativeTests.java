package com.worldline.interview.widgetmachine.engines;

import static com.worldline.widgetmachine.common.FuelType.COAL;
import static com.worldline.widgetmachine.common.FuelType.DIESEL;
import static com.worldline.widgetmachine.common.FuelType.PETROL;
import static com.worldline.widgetmachine.common.FuelType.WOOD;

import org.junit.Test;

import com.worldline.widgetmachine.WidgetMachine;
import com.worldline.widgetmachine.engines.SteamEngine;

public class SteamEngineNegativeTests {

	protected int fuelLevel;
	protected int numberOfWidgetsToBeProduced;

	public void setUp() {
		this.fuelLevel = 50;
		this.numberOfWidgetsToBeProduced = 11;
	}

	@Test(expected = IllegalStateException.class)
	public void testInitializeSteamWithUnsupportedFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(PETROL));
		widgetMachine.addFuelToEngine(PETROL, fuelLevel);
	}

	@Test(expected = IllegalStateException.class)
	public void testInitializeSteamWithUnsupportedFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(DIESEL));
		widgetMachine.addFuelToEngine(DIESEL, fuelLevel);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithWrongFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(WOOD));
		widgetMachine.addFuelToEngine(COAL, fuelLevel);
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithWrongFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(WOOD));
		widgetMachine.addFuelToEngine(PETROL, fuelLevel);
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithoutFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(WOOD));
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithoutFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new SteamEngine(COAL));
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}
}
