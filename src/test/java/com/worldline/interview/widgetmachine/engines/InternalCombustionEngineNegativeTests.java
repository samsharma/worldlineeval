package com.worldline.interview.widgetmachine.engines;

import static com.worldline.widgetmachine.common.FuelType.COAL;
import static com.worldline.widgetmachine.common.FuelType.DIESEL;
import static com.worldline.widgetmachine.common.FuelType.PETROL;
import static com.worldline.widgetmachine.common.FuelType.WOOD;

import org.junit.Test;

import com.worldline.widgetmachine.WidgetMachine;
import com.worldline.widgetmachine.engines.InternalCombustionEngine;

public class InternalCombustionEngineNegativeTests {

	protected int fuelLevel;
	protected int numberOfWidgetsToBeProduced;

	public void setUp() {
		this.fuelLevel = 50;
		this.numberOfWidgetsToBeProduced = 11;
	}

	@Test(expected = IllegalStateException.class)
	public void testInitializeSteamWithUnsupportedFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(WOOD));
		widgetMachine.addFuelToEngine(PETROL, fuelLevel);
	}

	@Test(expected = IllegalStateException.class)
	public void testInitializeSteamWithUnsupportedFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(COAL));
		widgetMachine.addFuelToEngine(DIESEL, fuelLevel);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithWrongFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(PETROL));
		widgetMachine.addFuelToEngine(DIESEL, fuelLevel);
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithWrongFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(PETROL));
		widgetMachine.addFuelToEngine(WOOD, fuelLevel);
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithoutFuel1() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(PETROL));
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}

	@Test(expected = IllegalStateException.class)
	public void testProductionWithoutFuel2() {
		WidgetMachine widgetMachine = new WidgetMachine(new InternalCombustionEngine(DIESEL));
		widgetMachine.produceWidgets(numberOfWidgetsToBeProduced);
	}
}
